# React Native Boiler Plate
This is a boiler plate for a react native project with the following tech baked in:
* react native
* mocha for testing
* eslint rules
* forced test coverage
* redux
* react router
